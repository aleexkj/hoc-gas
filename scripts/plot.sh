#!/bin/zsh
echo "Plot from file: ${1} (sequence of fitness values)"
cut -d ' ' -f 1 $1 > avg_pop.fitness
cut -d ' ' -f 2 $1 > best_it.fitness
cut -d ' ' -f 3 $1 > best_gb.fitness
gnuplot <<- EOF
        set size ratio 0.3
        set xlabel "Generation"
        set ylabel "Fitness"
        set title "Genetic Algorithms"
        set term svg background rgb 'white'
        set output "$1.svg"
        plot "avg_pop.fitness" title 'Avg Population' with lines, \
             "best_it.fitness" title 'Best of generation' with lines, \
             "best_gb.fitness" title 'Best solution' with lines
EOF
rm avg_pop.fitness
rm best_it.fitness
rm best_gb.fitness
echo "Saved into ${1}.svg file"
