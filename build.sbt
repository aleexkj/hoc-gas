import xerial.sbt.Pack._
packSettings
/* Packing */
packMain := Map("kct_genetic_algorithm" -> "mx.unam.ciencias.hoc.gas.Main")
packGenerateWindowsBatFile := false
packResourceDir += (baseDirectory.value / "data" -> "data")
packTargetDir := baseDirectory.value
packDir := "dist"
fork in (run) := true
/* Data */
organization := "mx.unam.ciencias.hoc"
name := "kct-genetic-algorithms"
version := "1.0"
/* Correct versions */
scalaVersion := "2.11.8"
/* Settings */
exportJars := true
/* Dependencies */
libraryDependencies ++= Seq(
	"com.typesafe.slick" %% "slick" % "3.1.1",
	"org.xerial" % "sqlite-jdbc" % "3.7.2",
	"org.scala-graph" %% "graph-core" % "1.11.2",
	"org.scalactic" %% "scalactic" % "3.0.0",
	"org.scalatest" %% "scalatest" % "3.0.0" % "test",
	"ch.qos.logback" %	"logback-classic" % "1.1.7",
	"com.typesafe.scala-logging" %% "scala-logging" % "3.5.0",
	"com.github.scopt" %% "scopt" % "3.5.0"
)
/* Find JARS for libs */
unmanagedBase <<= baseDirectory { base => base / "lib" }
/* Documentation */
sources in (Compile, doc) ~= (_ filter (_.getName != "Main.scala"))
target in Compile in doc := baseDirectory.value / "api"
autoAPIMappings := true
apiMappings += (
  (unmanagedBase.value / "gs-core-1.3.jar") ->
    url("http://graphstream-project.org/doc/")
)
/* test coverage */
coverageEnabled := true
coverageExcludedPackages := "<empty>;.*gui.*;.*KCTProblem.*"
// ensime
scalaVersion in ThisBuild := "2.11.8"
