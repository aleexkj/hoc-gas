package mx.unam.ciencias.hoc
package gas
package utils

import scala.util.Random

/**
 * Singleton object for managing the RNG
 */
object RNG {
  private var random = new Random()

  /**
   * Set seed to RNG
   * @param seed the new seed. RNG will be restarted
   */
  def setSeed(seed: Long): Unit = {
    random = new Random(seed)
  }

  /**
   * Choose int between  [0, value)
   * @param exclusiveEnd the value to limit the range
   * @return int between 0 (inclusive) and exclusiveEnd (exclusive)
   */
  def chooseInt(exclusiveEnd: Int): Int = {
    random.nextInt(exclusiveEnd)
  }

  /**
   * Wrapper to access [scala.util.Random] instance
   * @return float randomly choosen between 0.0 and 1.0
   */
  def nextFloat(): Float = {
    random.nextFloat()
  }

  /**
   * choose value with given probabilities
   *
   * element in values(i) must have probability
   * of being choosen in probability(i)
   *
   * the sum of the probabilities must equal 1.0
   * should be warned of Unexpected errors otherwise
   *
   * @param values list of elements to choose
   * @param probability probabilities correspondig to the values
   * @return a element in values randomly choosen
   */
  def choice[A](values: Array[A], probability: Array[Double]): A = {
    if (values.length != probability.length) {
      throw new IllegalArgumentException("Arrays must be of equal size")
    }

    var i = 0
    val rn: Double = random.nextDouble()
    var current_proba: Double = 0.0

    for(i <- 0 until values.length) {
      current_proba += probability(i)
      if (current_proba >= rn) {
        return values(i)
      }
    }

    throw new IllegalStateException("There is no way to choose element")
  }

  /**
   * shuffles in place the given array
   * @param values array to shuffle
   */
  def shuffle[A](values: Array[A]): Unit = {
    val list = random.shuffle(values.toList)
    var index = 0
    // copy in array
    for (value <- list) {
      values(index) = value
      index += 1
    }
  }
}
