package mx.unam.ciencias.hoc.gas.utils.profile

/**
 * Provides method to measure the time ellapsed at runtime
 */
object Timing {
  def time[R](block: => R): Long = {
    val t0 = System.currentTimeMillis()
    val result = block    // call-by-name
    val t1 = System.currentTimeMillis()
    (t1 - t0)
  }
}
