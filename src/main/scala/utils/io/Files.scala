package mx.unam.ciencias.hoc.gas.utils.io

import scala.io.Source
import java.io._

/**
 * Retrieves data in differents formats from files
 */
object Files {
  /**
   * Reads values that are separated from file
   * @param filename the file path to read
   * @param separator the separator characters
   * @return an array with the values found
   */
  def readSeparatedNumbers(filename: String, separator: String = ", "): Array[String] = {
    Files.readAsOneString(filename).split(separator)
  }

  /**
   * Open a stream to write a file
   * @param filename the file to open
   * @return stream to the file
   */
  // $COVERAGE-OFF$
  def openForWrite(filename: String): PrintWriter = {
    val writer = new PrintWriter(new File(filename))
    writer
  }

  /**
   * Reads a file and return it all his data in one string
   * @param filename the file to read
   * @return a string with the contents of the file or an empty string if failed to read
   */
  def readAsOneString(filename: String): String = {
    var file_string = ""
    try {
      val buffer = Source.fromFile(filename)
      file_string = buffer.getLines.mkString
      buffer.close
    } catch {
      case e: Exception => throw new Exception("Can't open file")
    }

    file_string
  }
}
