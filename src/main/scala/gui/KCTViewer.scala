package mx.unam.ciencias.hoc
package gas
package gui

import org.graphstream.graph._
import java.util.Iterator
import kct.KCardinalityTree

/**
 * Provides methods to format a given Graph.
 * All changes in a Graph wold be seen by GUI.
 */
object KCTViewer {

  /**
   * Adds labels for weights in the graph
   * @param graph the graph to format
   */
  def format(graph: Graph): Unit = {
    val iter_edges: Iterator[Edge] = graph.getEdgeIterator[Edge]()
    while(iter_edges.hasNext) {
      val edge: Edge = iter_edges.next()
      edge.setAttribute("ui.label", f"${edge.getNumber("weight")}%.2f")
    }
  }

  /**
   * Removes all styles in the graph
   * @param graph the target graph
   */
  def clean(graph: Graph): Unit = {
    var iter_edges: Iterator[Edge] = graph.getEdgeIterator[Edge]()
    while(iter_edges.hasNext()) {
      val edge: Edge = iter_edges.next()
      edge.setAttribute("ui.class", "")
    }
  }

  /**
   * Draws all the edges covered by a tree in a population
   * @param graph the graph to format
   * @param population the population source
   */
  def drawCoverage(graph: Graph, population: Array[_ <:Graph]): Unit = {
    for (tree <- population) {
      val it: Iterator[Edge] = tree.getEdgeIterator[Edge]()
      while (it.hasNext) {
        val edge_kct = it.next()
        val edge = graph.getEdge[Edge](edge_kct.getId()) // will be the same
        edge.setAttribute("ui.class", "marked")
      }
    }
  }

  /**
   * Draws the tree given in the graph
   * @param graph the graph to format
   * @param tree the tree to draw
   */
  def drawCurrentBest(graph: Graph, tree: KCardinalityTree): Unit = {
    val it: Iterator[Edge] = tree.getEdgeIterator[Edge]()
    while (it.hasNext) {
      val edge_kct = it.next()
      val edge = graph.getEdge[Edge](edge_kct.getId()) // will be the same
      edge.setAttribute("ui.class", "best")
    }
  }
}
