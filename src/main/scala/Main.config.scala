package mx.unam.ciencias.hoc.gas

/**
 * Represents the config parameters of the project
 */
private case class Config(
  gui: Boolean = false, // if show visual of problem
  labels: Boolean = false,
  seed: Long = Constants.SEED,
  k: Int = Constants.MIN_CARD, // k for cardinality
  input: String = "",
  output: String = "",
  save: Boolean = false,
  nodes: Int = Constants.DEFAULT_SIZE
)

/**
 * Args parser, builds and validates the args passed to the program
 */
// $COVERAGE-OFF$
private class GetOpts() extends scopt.OptionParser[Config]("kct_genetic_algorithm") {
  head("kct-gas version 1.0 (c) kreiz")
  help("help").text("Show this help text")
  version("version").text("Show version")

  /** Graphical **/
  opt[Unit]('g', "gui")
  .optional()
  .text("Wether to show GUI or not")
  .action((_, c) => c.copy(gui=true))

  opt[Unit]('l', "labels")
  .optional()
  .text("Wether to show labels for weights or not")
  .action((_, c) => c.copy(labels=true))

  /** Required **/
  arg[Long]("<seed>")
  .required()
  .text("RNG seed")
  .action((x, c) => c.copy(seed = x))

  arg[Int]("<k_cardinality>")
  .required()
  .text("Cardinality of tree to look for")
  .action((x, c) => c.copy(k = x))
  .validate(x => if (x > Constants.MIN_CARD) success else failure("Not a valid cardinality"))

  /** Problem **/
  opt[String]('i', "input")
  .optional()
  .valueName("<dgs-file-path>")
  .text("DGS file to load graph")
  .action((x, c) => c.copy(input = x, nodes=0))
  .validate(x => if (x.endsWith(".dgs")) success else failure("Invalid format for input"))

  opt[String]('o', "output")
  .optional()
  .valueName("<file-prefix>")
  .text("If passed saves the k-card tree found and the graph source")
  .action((x, c) => c.copy(output=x, save=true))

  opt[Int]('n', "nodes")
  .optional()
  .valueName("<nodes-graph>")
  .text("Indicates how many nodes are in the graph source generated. (Default: 100)")
  .action((x, c) => c.copy(nodes=x))
  .validate(x => if (x > 1) success else failure("Invalid number of nodes"))

  override def showUsageOnError: Boolean = true
}

/**
 * Constans predefined to the Genetic Algorithm
 */
// $COVERAGE-OFF$
object Constants {
  /***********************
   * Fixed Parameters
   * *********************/

  /**
    Min size for the population
   */
  val MIN_SIZE = 50.0

  /**
    Max size for the population
   */
  val MAX_SIZE = 200.0

  /**
   * Min wight for edges
   */
  val MIN_WEIGHT = 1

  /**
   * Max weight for edges
   */
  val MAX_WEIGHT = 100


  /**********************
   * Defaults to CLI
   * ********************/

  /**
   * Min cardinality to look for
   */
  val MIN_CARD = 2

  /**
   * Default seed for RNG
   */
  val SEED: Long = 13

  /**
   * Default size of graph
   */
  val DEFAULT_SIZE = 100

  /***********************
   * Custom parameters
   * *********************/

  /**
   * Max value for probability in Prim
   */
  val MAX_PDET = 0.85

  /**
   * Min value for probability in Prim
   */
  val MIN_PDET = 0.5

  /**
   * Max age for individuals to survive
   */
  val MAX_AGE = 10

  /**
   * Max iterations to try
   */
  val MAX_ITERATIONS = 1000

  /**
   * Times the best solution can be accepted
   */
  val MAX_REPEATED = 3

  /**
   * Value used in fitness function
   */
  val C = 100
}
