package mx.unam.ciencias.hoc.gas

import com.typesafe.scalalogging.Logger
import gui.KCTViewer
import utils.RNG
import org.graphstream.graph.{ Node, Edge, Graph }
import org.graphstream.graph.implementations.SingleGraph
import components.PopulationOps
import PopulationOps.Population
import kct.GraphUtils
import kct.KCTProblem
import kct.KCardinalityTree


// $COVERAGE-OFF$
object Main {
  private val logger = Logger("hoc-ga-kct")

  def main(args: Array[String]): Unit = {
    val parser = new GetOpts()

    parser.parse(args, Config()) match {
      case Some(config) => // Config found in parameters
        RNG.setSeed(config.seed)

        logger.info("Building graph..")
        var g: SingleGraph = if (config.nodes==0) GraphUtils.readGraph(config.input) else GraphUtils.generateRandom(config.nodes)
        if (config.save) {
          g.write(config.output + "_graph.dgs")
        }
        logger.info("Done")

        // Events
        var generationChange = (pop: Population[_ <:Graph], kct: KCardinalityTree) => {}
        if (config.gui) {
          generationChange = (pop: Population[_ <:Graph], kct: KCardinalityTree) => {
            KCTViewer.clean(g)
            if (config.labels) {
              KCTViewer.format(g)
            }
            Thread.sleep(200)
            KCTViewer.drawCoverage(g, pop)
            KCTViewer.drawCurrentBest(g, kct)
            Thread.sleep(1000)
          }
          g.display()
        }

        val instance = new KCTProblem(g, config.k, generationChange)
        logger.debug(instance.toString)
        instance.run()

        val solution = instance.getBest
        logger.info(s"Solution (id - age - fitness): $solution")
        if (config.save) {
          solution.write(config.output + "_solution.dgs")
        }

      case None => // GetOpts handle the usage text
    }
  }
}
