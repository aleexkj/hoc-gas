package mx.unam.ciencias.hoc
package gas
package kct

import java.util.Iterator
import scala.collection.mutable.HashSet
import scala.collection.mutable.HashMap
import scala.collection.mutable.TreeSet
import gas.components.{ Solution, SolutionFactory }
import gas.components.PopulationOps.Population
import gas.Constants._
import gas.utils.RNG
import org.graphstream.graph._
import org.graphstream.graph.implementations.SingleGraph

/**
 * Factory to generate random k-cardinality trees
 * @param graph space to build trees
 * @param k cardinality of the trees
 */
class KCTFactory(graph: Graph, k: Int)
  extends SolutionFactory[KCardinalityTree] {

  /**
   * Generates random k-cardinality tree
   * It runs prim starting on a randomly choosen node
   * @return [[KCardinalityTree]] generated
   */
  override def getRandom(): KCardinalityTree = {
    val node_init: Node = this.graph.getNode(RNG.chooseInt(this.graph.getNodeCount))
    newFromNode(node_init)
  }

  // runs Prim
  private def newFromNode(node: Node): KCardinalityTree = {
    val generator = new Prim(graph)
    generator.init(Left(node))

    val kct = new KCardinalityTree(f"${node.getId()}", this.k)
    while (kct.getEdgeCount < this.k) {
      val edge = generator.next()
      kct.addFromEdge(edge)
    }

    if (!GraphUtils.isTree(kct)) {
      throw new InstantiationException("Unexpected construction")
    }

    kct
  }

  /**
   * Takes different start points and generate trees from them
   * @param n the number of trees being generated
   * @return population with n randomly generated trees
   */
  def generateRandom(n: Int): Population[KCardinalityTree] = {
    val nodes: Array[Int] = (0 until graph.getNodeCount()).toArray
    RNG.shuffle[Int](nodes)
    val random = new Array[KCardinalityTree](n)

    var index = 0
    for(node_init <- nodes.take(n)) {
      random(index) = newFromNode(this.graph.getNode(node_init))
      index += 1
    }

    random
  }
}

/**
 * Represents a solution for the K-Cardinalty Tree problem
 * It is build on top of a graph
 * Offers methods to manipulate it and getters for his attributes
 */
class KCardinalityTree(id: String, cardinality: Int)
  extends SingleGraph (id, false, true, cardinality + 1, cardinality)
  with Solution {

  /**
   * Inital fitness of graph
   */
  var fitness: Double = 0.0
  /**
   * Initial age of graph
   */
  var age: Int = 0

  /**
   * Adds an edge to the graph and update fitness
   * @param id the if of the edge
   * @param source the source node of the edge
   * @param target the target node of the edge
   * @param weight the weight of the edge
   * @return Edge added in the graph
   */
  def addEdge(id: String, source: String, target: String, weight: Double): Edge = {
    val edge: Edge = super.addEdge(id, source, target)
    this.fitness += weight
    edge.setAttribute("weight", weight.asInstanceOf[java.lang.Double])
    edge
  }

  /**
   * Add an edge copying another
   * @param edge the edge to copy into this object
   * @return edge added in the graph
   */
  def addFromEdge(edge: Edge): Edge = {
    val node1: Node = edge.getNode1()
    val node0: Node = edge.getNode0()
    val weight: Double = edge.getAttribute("weight")
    val edge_created: Edge = this.addEdge(edge.getId(), node0.getId(), node1.getId(), weight)
    edge_created
  }

  /**
   * Returns the current fitness of this graph
   * @return fitness as the sum of weights in this graph
   */
  override def getFitness(): Double = {
    C + (this.fitness - this.cardinality * MAX_WEIGHT)
  }

  /**
   * Increments the age of this graph
   */
   def makeOlder: Unit = this.age += 1

   /**
    * Resets the age of this graph
    */
   def resetAge: Unit = this.age = 0

  /**
   * Mutate this graph
   */
  override def mutate(p: Double): Unit = throw new NotImplementedError("KCardinalityTree can't be mutated")

  /**
   * Crossover this graph with another
   * @param mate the graph to crossover with
   * @return offspring resulting of recombination
   */
   def crossoverWith[T, G](that: T): G = {
    if (!that.isInstanceOf[Graph]) {
      throw new NotImplementedError("Crossover must be with a Graph instance")
    }

    val mate = that.asInstanceOf[Graph]

    if (GraphUtils.intersect(this, mate).getEdgeCount() == 0) {
      throw new UnsupportedOperationException("Trees must be overlapped at least with one edge")
    }
    val space = GraphUtils.union(this, mate)
    val edge = GraphUtils.getMinEdge(space)
    val prim = new Prim(space)
    val child = new KCardinalityTree(f"${this.getId()}c", this.cardinality)

    prim.init(Right(edge))
    child.addFromEdge(edge)
    while (child.getEdgeCount < this.cardinality) {
      val edge = prim.next()
      child.addFromEdge(edge)
    }

    if (!GraphUtils.isTree(child)) {
      throw new InstantiationException("Unexpected creation")
    }

    return child.asInstanceOf[G]
  }

  override def equals(that: Any): Boolean = {
    if (that.isInstanceOf[KCardinalityTree]) {
      val it1: Iterator[Edge] = this.getEdgeIterator[Edge]()
      val it2: Iterator[Edge] = that.asInstanceOf[KCardinalityTree].getEdgeIterator[Edge]()
      val idsEdges1 = new HashSet[String]()
      val idsEdges2 = new HashSet[String]()

      while (it1.hasNext) {
        idsEdges1 += it1.next().getId()
      }

      while (it2.hasNext) {
        idsEdges2 += it2.next().getId()
      }

      return idsEdges1 == idsEdges2 &&
              this.getFitness() == that.asInstanceOf[KCardinalityTree].getFitness()
    }

    return false
  }

  /**
   * Return the number of nodes in this graph
   */
  def nodes: Int = this.getNodeCount()
  /**
   * Return the number of edges in this graph
   */
  def edges: Int = this.getEdgeCount()
  /**
   * Return the cardinality with of this graph was created
   */
  def kCardinality: Int = this.cardinality
  /**
   * Return current age of this graph
   */
  def getAge: Int = this.age

  /**
   * String representation
   * @return the id of this graph
   */
  override def toString: String = f"${this.getId()} - $age - $fitness%.4f"
}
