package mx.unam.ciencias.hoc
package gas
package kct

import org.graphstream.graph._
import java.util.Iterator
import scala.collection.mutable.HashMap
import scala.collection.mutable.TreeSet
import gas.utils.RNG
import gas.Constants._

object Prim {
  /**
   * Ordering for nodes based on their weight
   * @param weights map for nodes with their weights
   */
   def weightedNodes(weights: HashMap[String, Double]): Ordering[Node] = new Ordering[Node] {
    /* if two nodes have the same id then are the same,
    if one node have lower weight then is high priority */
    override def compare(nodeA: Node, nodeB: Node): Int = {
      var order = 1
      if (nodeA.getId() == nodeB.getId()) {
        order = 0
      } else if (weights(nodeA.getId()) <= weights(nodeB.getId())) {
        order = -1
      }
      order
    }
  }

  /**
   * Ordering for edges based on their weight
   * @param weights map for edges with their weights
   */
   def weightedEdges(weights: HashMap[String, Double]): Ordering[Edge] = new Ordering[Edge] {
    /* if two edges have the same id then are the same,
    if one edge have lower weight then is high priority */
    override def compare(edgeA: Edge, edgeB: Edge): Int = {
      var order = 1
      if (edgeA.getId() == edgeB.getId()) {
        order = 0
      } else if (weights(edgeA.getId()) <= weights(edgeB.getId())) {
        order = -1
      }
      order
    }
  }
}
/**
 *
 * Offer methods to build trees based on Prim Algorigthm for MST
 * Outline:
 * Choose a random vertex to start and create tree T
 * until k edges reached:
 * adds the min edge of T.E \ G.E such that
 * it has exactly one vertex in T
 */
class Prim(g: Graph) {
  /**
   * HashMap to represent weight function
   * Keeps the nodeId -> weight for nodes visited
   */
  private val weights = new HashMap[String, Double]()
  /**
   * HashMap to represent edges for a vertex
   * Keeps the nodeId -> nodeTargetId for nodes choosen
   */
  private val edges = new HashMap[String, Edge]()
  /**
   * Queue to get min weighted vertex in each step
   * It's build with a Red-black tree so each operation is O(log G.V)
   */
  private implicit val ordering: Ordering[Node] = Prim.weightedNodes(weights)
  private val queue = new TreeSet[Node]()

  /**
   * Probability to pick an edge in the process
   * Takes lower and upper bounds so it is predecible
   */
  private val p_det = (MAX_PDET - MIN_PDET) * RNG.nextFloat() + MIN_PDET
  /**
   * Initializes the generator starting in one node or edge
   * 
   * If node is given, it adds the node to the graph and a neighbors to candidates
   * If edge is given, it adds the edge to the graph and neighbors of both nodes
   * @param Eiter[Node, Edge] to start
   */
  def init(start: Either[Node, Edge]): Unit = {
    start match {
      case Left(node: Node) =>
        addInitialNode(node)
      case Right(edge: Edge) =>
        addInitialEdge(edge)
    }
  }

  private def addInitialNode(node: Node, weight: Double = 0.0, addEdgeIf: Node => Boolean = _ => true): Unit = {
    this.weights += node.getId() -> weight

    val iter_edges: Iterator[Edge] = node.getEachEdge().iterator()
    while (iter_edges.hasNext()) {
      val edge = iter_edges.next()
      val opposite: Node = edge.getOpposite(node)
      val opposite_id: String = opposite.getId()
      val current_weight: Double = edge.getAttribute[Double]("weight")
      if (addEdgeIf(opposite)) {
        // it has been already visited
        if (this.weights.contains(opposite_id)) {
          // not in T and lower weight
          if (current_weight < this.weights(opposite_id) && this.queue(opposite)) {
            this.queue -= opposite // drop it in order to update
            this.weights.update(opposite_id, current_weight)
            this.edges.update(opposite_id, edge)
            this.queue += opposite // add again with new weight
          }
        } else { // discover edge
          this.weights += opposite_id -> current_weight
          this.edges += opposite_id -> edge // node from where is reached
          this.queue += opposite
        }
      }
    }
  }

  private def addInitialEdge(edge: Edge): Unit = {
    val node0: Node = edge.getNode0()
    val node1: Node = edge.getNode1()

    addInitialNode(node0, addEdgeIf = (n: Node) => n.getId() != node1.getId())
    addInitialNode(node1, edge.getAttribute("weight"), (n: Node) => n.getId() != node0.getId())
  }

  private def pickNode(): Node = {
    var probability = RNG.nextFloat()
    var current_node = this.queue.min

    // get all other options
    var mins: TreeSet[Node] = this.queue.clone()
    mins -= current_node

    while (probability > this.p_det && mins.size >= 1) { // choose "random"
      probability = RNG.nextFloat()
      current_node = mins.min
      mins -= current_node
    }

    current_node
  }

  /**
   * Return the next edge for the graph
   * @return edge picked
   */
  def next(): Edge = {
    val current_node = pickNode()
    val current_id = current_node.getId()
    this.queue -= current_node

    val edge_picked = this.edges(current_id)
    var iter_edges: Iterator[Edge] = current_node.getEachEdge().iterator()
    while (iter_edges.hasNext()) {
      val edge = iter_edges.next()
      val opposite: Node = edge.getOpposite(current_node) // neighbor
      val opposite_id: String = opposite.getId()
      val current_weight: Double = edge.getAttribute("weight") // edge weight

      // it has been already visited
      if (this.weights.contains(opposite_id)) {
        // not in T and lower weight
        if (current_weight < this.weights(opposite_id) && this.queue(opposite)) {
          this.queue -= opposite // drop it in order to update
          this.weights.update(opposite_id, current_weight)
          this.edges.update(opposite_id, edge)
          this.queue += opposite // add again with new weight
        }
      } else { // discover edge
        this.weights += opposite_id -> current_weight
        this.edges += opposite_id -> edge // node from where is reached
        this.queue += opposite
      }
    }

    edge_picked
  }
}
