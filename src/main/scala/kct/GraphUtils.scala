package mx.unam.ciencias.hoc
package gas
package kct

import java.util.Iterator
import gas.utils.io._
import org.graphstream.graph._
import org.graphstream.graph.implementations._
import org.graphstream.algorithm.generator._
import scala.collection.mutable.HashMap
import scala.collection.mutable.HashSet
import scala.collection.mutable.TreeSet

/**
 * Offers utilities for graphs
 */
object GraphUtils {
  /**
   * Get the union of two graphs:
   * ``(G1.V U G2.V, G1.E U G2.E)``
   * @param  g1 first graph
   * @param  g2 second graph
   * @return a graph as described above
   */
  def union(g1: Graph, g2: Graph): Graph = {
    val graph = new SingleGraph(f"(${g1.getId()}) union (${g2.getId()})", false, true)

    val it1: Iterator[Edge] = g1.getEdgeIterator[Edge]()
    while (it1.hasNext()) {
      val edge: Edge = it1.next()
      val node0 = edge.getNode0[Node]()
      val node1: Node = edge.getNode1()
      val edge_created: Edge = graph.addEdge(edge.getId(), node0.getId(), node1.getId())
      edge_created.setAttribute("weight", edge.getAttribute("weight"))
    }

    val it2: Iterator[Edge] = g2.getEdgeIterator[Edge]()
    while (it2.hasNext()) {
      val edge: Edge = it2.next()
      val node0: Node = edge.getNode0()
      val node1: Node = edge.getNode1()
      val edge_created: Edge = graph.addEdge(edge.getId(), node0.getId(), node1.getId())
      edge_created.setAttribute("weight", edge.getAttribute("weight"))
    }

    graph
  }

  /**
   * Computes the intersection (edges) of two graphs:
   * Will get the edges of the intersection and the vertices of them
   * @param  g1 first graph
   * @param  g2 second graph
   * @return the graph resulting of the intersection
   */
  def intersect(g1: Graph, g2: Graph): Graph = {
    val edges = new HashSet[String]()
    val graph = new SingleGraph(f"${g1.getId()} intersect ${g2.getId()}", false, true)

    val it1: Iterator[Edge] = g1.getEdgeIterator[Edge]()
    while (it1.hasNext()) {
      val edge: Edge = it1.next()
      edges += edge.getId()
    }

    val it2: Iterator[Edge] = g2.getEdgeIterator[Edge]()
    while (it2.hasNext()) {
      val edge: Edge = it2.next()
      if (edges contains edge.getId()) {
        val node0: Node = edge.getNode0()
        val node1: Node = edge.getNode1()
        val edge_created: Edge = graph.addEdge(edge.getId(), node0.getId(), node1.getId())
        edge_created.setAttribute("weight", edge.getAttribute("weight"))
      }
    }

    graph
  }

  /**
   * Returns the lower edge based on their weightedNodes
   * @param graph graph to search
   * @return an Edge with min weight from graph
   */
  def getMinEdge(graph: Graph): Edge = {
    if (graph.getEdgeCount() < 1) {
      throw new NoSuchElementException("Graph has no edges")
    }

    var min = graph.getEdge[Edge](0)
    var min_weight: Double = min.getAttribute("weight")
    val iter_edges: Iterator[Edge] = graph.getEdgeIterator[Edge]()
    while (iter_edges.hasNext()) {
      val edge: Edge = iter_edges.next()
      val weight: Double = edge.getAttribute("weight")
      if (min_weight > weight) {
        min = edge
        min_weight = weight
      }
    }

    min
  }


  /**
   * Generates a random graph by Dorogovtsev Mendes algorithm
   * @param n number of nodes in the graph
   * @return new Graph with n nodes
   */
  def generateRandom(n: Int): SingleGraph = {
    val graph = new SingleGraph("dorogovtsev")
    graph.addAttribute("ui.stylesheet", Files.readAsOneString("data/style.css"))

    val gen = new DorogovtsevMendesGenerator()
    gen.addEdgeAttribute("weight")
    gen.setEdgeAttributesRange(Constants.MIN_WEIGHT, Constants.MAX_WEIGHT)
    gen.addSink(graph)
    gen.begin()
    for (_ <- 0 until (n - 3)) {
      gen.nextEvents()
    }
    gen.end()

    graph
  }

  /**
   * Reads a graph from DGS file
   * @param filename the DGS file to read
   * @return SingleGraph from file read
   */
  def readGraph(filename: String): SingleGraph = {
    var name = filename.split(".dgs")(0)
    var graph = new SingleGraph(s"$name")
    graph.read(filename)
    graph
  }

  /**
   * Test if given graph is a tree
   * @param graph the graph to test
   * @return wether or not graph is a tree
   */
  def isTree(graph: Graph): Boolean = {
    val nodes = graph.getNodeCount()

    if (nodes == 0) {
      return true
    }

    val node: Node = graph.getNode(0)
    val iter_nodes: Iterator[Node] = node.getBreadthFirstIterator()
    var nodes_count = 0
    while(iter_nodes.hasNext()) {
      iter_nodes.next()
      nodes_count += 1
    }

    nodes_count == nodes && (nodes - 1) == graph.getEdgeCount()
  }

  /**
   * Extends one graph so it overlaps another
   * @param g1 the graph to be extended
   * @param g2 the graph looking for to be overlapped
   * @param space the graph from where edges are taken
   * @return graph extended
   */
  def extendToContain(g1: Graph, g2: Graph, space: Graph): Graph = {
    val intersection = intersect(g1, g2)
    if (intersection.getEdgeCount() > 0) {
      return g1
    }

    // build tree with edge not in g1 but in space, add in order based on weights
    val weights = new HashMap[String, Double]()
    implicit val ordering: Ordering[Edge] = Prim.weightedEdges(weights)
    val queue = new TreeSet[Edge]()

    // init: add all edges neighbors from g1
    var it: Iterator[Node] = g1.getNodeIterator[Node]()
    while (it.hasNext) { // visit edges ady to each node
      val node = it.next()
      val node_space = space.getNode[Node](node.getId())
      if (node_space != null) { // compatible in space
        val sit: Iterator[Edge] = node_space.getEachEdge[Edge]().iterator() // ady
        while (sit.hasNext) {
          val edge = sit.next()
          val weight: Double = edge.getAttribute("weight")
          if (g1.getEdge[Edge](edge.getId()) == null) { // not already in g1
            weights += edge.getId() -> weight
            queue += edge
          }
        }
      }
    }

    // copy so its possible to alter space
    val copy = new SingleGraph(g1.getId(), false, true)
    val itc = g1.getEdgeIterator[Edge]()
    while (itc.hasNext) {
      adddEdgeFromEdgeToGraph(copy, itc.next())
    }


    // run
    var edge = queue.min
    adddEdgeFromEdgeToGraph(copy, edge)
    queue -= edge

    // stop when there is edge from g2
    while (g2.getEdge[Edge](edge.getId()) == null && queue.size > 0) {
      // add neighbors from edge
      val fst_node = edge.getNode0[Node]()
      val snd_node = edge.getNode1[Node]()

      var iter_edges = fst_node.getEachEdge[Edge]().iterator()
      while (iter_edges.hasNext) {
        val neighbor_edge = iter_edges.next()
        if (!weights.contains(neighbor_edge.getId())) { // not already visited
          val weight: Double = neighbor_edge.getAttribute("weight")
          weights += neighbor_edge.getId() -> weight
          queue += neighbor_edge
        }
      }

      iter_edges = snd_node.getEachEdge[Edge]().iterator()
      while (iter_edges.hasNext) {
        val neighbor_edge = iter_edges.next()
        if (!weights.contains(neighbor_edge.getId())) { // not already visited
          val weight: Double = neighbor_edge.getAttribute("weight")
          weights += neighbor_edge.getId() -> weight
          queue += neighbor_edge
        }
      }

      edge = queue.min // choose next
      adddEdgeFromEdgeToGraph(copy, edge)
      queue -= edge
    }

    if (queue.size == 0) { // no more edges and no intersection
      throw new UnsupportedOperationException("There aren't more edges to extend")
    }

    copy
  }

  private def adddEdgeFromEdgeToGraph (graph: Graph, edge: Edge): Unit = {
    val weight: Double = edge.getAttribute("weight")
    val edge_added: Edge = graph.addEdge(edge.getId(), edge.getNode0[Node]().getId(), edge.getNode1[Node]().getId());
    edge_added.setAttribute("weight", weight.asInstanceOf[java.lang.Double])
  }

}
