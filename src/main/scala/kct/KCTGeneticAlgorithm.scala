package mx.unam.ciencias.hoc
package gas
package kct

import Constants._
import gas.components._
import PopulationOps.Population
import gas.utils.RNG
import gas.utils.io.Files
import gas.utils.profile.Timing.time
import scala.math.min
import scala.math.max
import scala.collection.mutable.MutableList
import org.graphstream.graph._
import com.typesafe.scalalogging.Logger

/**
 * Represents a genetic algorithm for the KCT problem
 * Components:
 * - Generational replacement
 * - Proportional selection
 * - Age factor instead of mutation
 * - Stop condition as times the best solution is reached
 */
class KCTProblem(
  space: Graph,
  k: Int,
  generationChange: (Population[_ <:Graph], KCardinalityTree) => Unit) {

  private var size: Int = min(this.space.getNodeCount, min(MAX_SIZE , max(this.space.getEdgeCount / this.k, MIN_SIZE)).toInt)
  private val factory = new KCTFactory(space, k)
  private val selection = SurvivalMethodFactory.generational[KCardinalityTree, KCTFactory](factory)
  private var population = this.factory.generateRandom(this.size)
  private var best: Option[KCardinalityTree] = None
  private var best_repeated = 0

  private val logger = Logger("ga-run")
  private val file_output = Files.openForWrite(space.getId() + "_output")
  logger.info(s"Saving plot data in ${space.getId()}_output file")

  /**
   * Runs this genetic algorithm
   */
  def run(): Unit = {
    var generation = 0
    this.onGenerationChange()

    val ellapsed_time = time {
      while (generation < MAX_ITERATIONS && best_repeated < MAX_AGE * MAX_REPEATED) {
        /* crossover */
        val mating_pool = SelectionMethods.proportionalSelection(this.population)
        var new_individuals = new MutableList[KCardinalityTree]()
        for (individual <- this.population) { // get new population
          val offspring = this.doCrossover(individual, mating_pool)
          if (!new_individuals.contains(offspring)) {
            offspring.makeOlder
            new_individuals += offspring
          }
        }
        /* Age factor */
        new_individuals = new_individuals.filter((k: KCardinalityTree) => k.getAge < MAX_AGE)
        /* selection */
        this.population = selection.survive(new_individuals, this.population) // introduce new material
        /* next generation */
        generation += 1
        this.onGenerationChange()
      }
    }

    file_output.close()
    logger.info(s"Ellapsed time: $ellapsed_time ms")
    logger.info(s"Generations: $generation")
  }

  private def doCrossover(individual: KCardinalityTree, mating_pool: SelectionMethods.MatingPool[KCardinalityTree]): KCardinalityTree = {
    // get mate from mating poool
    var mate = RNG.choice[KCardinalityTree](mating_pool._1, mating_pool._2)
    while (mate == individual)
      mate = RNG.choice[KCardinalityTree](mating_pool._1, mating_pool._2)
    // make sure it is compatible
    val compatible_mate = GraphUtils.extendToContain(mate, individual, this.space)
    // crossover it
    val offspring = individual.crossoverWith[Graph, KCardinalityTree](compatible_mate)
    // return best
    if (offspring.getFitness() < individual.getFitness()) {
      this.population.find((k: KCardinalityTree) => k == offspring) match {
        case Some(tree) =>
          return tree
        case None =>
          return offspring // new one, better one
      }
    } else {
      return individual // i guess im stick with u
    }
  }

  /**
   * Returns the best solution found
   * @return [[KCardinalityTree]] found
   */
  def getBest: KCardinalityTree =  best match {
    case Some(t) => t
    case None => throw new NoSuchElementException("There is no solution found")
  }

  private def updateBest(tree: KCardinalityTree): Unit = this.synchronized {
    best match {
      case Some(t) =>
        if (t.getFitness() > tree.getFitness()) {
          best = Some(tree)
          best_repeated = 0
          logger.info(f"Best solution updated: ${tree.getFitness()}%.4f")
        } else  if (t.getFitness() == tree.getFitness()) {
          best_repeated += 1
        }
      case None =>
        best = Some(tree)
        logger.info(f"Best solution updated: ${tree.getFitness()}%.4f")
    }
  }

  /* Events */
  private def onGenerationChange(): Unit = {
    val tree = PopulationOps.getBest(this.population)
    this.updateBest(tree)

    var avg = 0.0
    for (kct <- this.population) {
      avg += kct.getFitness()
    }
    avg = avg / this.size

    file_output.println(f"$avg%.4f ${tree.getFitness()}%.4f ${this.getBest.getFitness()}%.4f")
    file_output.flush()

    this.generationChange(this.population, tree)
  }

  override def toString: String = f"${space.getId()} G: ${space.getNodeCount()} V - ${space.getEdgeCount()} E :: $k-card :: #$size"
}
