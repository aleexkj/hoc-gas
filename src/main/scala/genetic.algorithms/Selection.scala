package mx.unam.ciencias.hoc
package gas
package components

import scala.reflect.ClassTag
import PopulationOps.Population
import gas.utils.RNG

/**
 * Factory to use selection methods
 *
 * Provides methods to use Tournament and Proportional Selection
 * each one receives the population from where parents are selected
 * and returns the set of parents with equal size than the population
 * and the probabilities to be picked.
 *
 * With them, you can select parents simply by
 *
 * ``RNG.choice(MatingPool)``
 */
object SelectionMethods {
  /**
   * Represents the mating pool
   * (Parents, Probability)
   *
   * Parents selected and their future probability weight
   */
  type MatingPool[A<:Solution] = (Array[A], Array[Double])

  /**
   * Returns selected parents based on tournament strategy
   *
   * t individuals compete and the best is passed to the mating pool
   *
   * @param population population to be selected
   * @param t the tournament size
   * @return a [[MatingPool]] composed with the winners
   */
  def tournamentSelection[B<:Solution : ClassTag](population: Population[B], t: Int): MatingPool[B] = {
    val selected = new Array[B](population.length)
    val probabilities = Array.fill[Double](population.length)(1.0 / population.length) // uniform

    for (index <- 0 until population.length) {
      selected(index) = tournament[B](population, t)
    }

    (selected, probabilities)
  }

  /**
   * Represents the tournament between t individuals
   * Auxiliar for selectByTournament
   *
   * @return best individual (winner)
   */
  private def tournament[B<:Solution : ClassTag](population: Population[B], t: Int): B = {
    if (population.length < t) {
      throw new NoSuchElementException("Not enough individuals in population")
    }

    // tournament size
    val toCompete = new Array[B](t)
    for (index <- 0 until t) {
      // randomly pick t elements from population
      toCompete(index) = population(RNG.chooseInt(population.length))
    }

    PopulationOps.getBest[B](toCompete)
  }

  /**
   * Represents the proportional selection
   * Each individual gets proportional probability respect to the total fitness
   *
   * @param solutions solutions to be selected
   * @return a [[MatingPool]] composed with all individuals and weighted probability
   */
  def proportionalSelection[B<:Solution : ClassTag](solutions: Population[B]): MatingPool[B] = {
    val selected = new Array[B](solutions.length)
    val probabilities = new Array[Double](solutions.length)

    var total_fitness: Double = 0.0
    for (index <- 0 until solutions.length) {
      val solution = solutions(index)
      /* Take the inverse so small values become greater values and have more probability */
      total_fitness += 1 / solution.getFitness()
      selected(index) = solution
    }
    RNG.shuffle(selected)
    // proportional
    for (index <- 0 to selected.length - 1) {
      val solution = selected(index)
      probabilities(index) = (1 / solution.getFitness()) / total_fitness
    }

    (selected, probabilities)
  }
}
