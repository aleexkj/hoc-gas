package mx.unam.ciencias.hoc.gas.components

/**
 * Factory to get random solutions
 *
 * When using this package the [[Solution]] and an instance of
 * this class must be generated
 */
trait SolutionFactory[A<:Solution] {
  /**
   * Creates a random solution
   * @return new solution randomly built
   */
  def getRandom(): A
}

/**
 * Represents an individual in the population
 *
 * A solution has a mutation and crossover operators
 * A solution has fitness function to evaluate his performance
 * A solution T is lower than B if T has lower fitness.
 * All the operations are made thinking in minimize fitness, so
 * if you want to maximize you should return the inverse in getFitness()
 */
trait Solution extends Ordered[Solution] {
  /**
   * Offers the mutation operator for a given solution
   * it should alter his chromosome by a probability
   * @param  probability the probability of being mutated
   */
  def mutate(probability: Double)

  /**
   * Offers the crossover operator for a given solution
   * A mate is a compatible solution with this object
   * and both recombine and produce an offspring
   * @param  mate  A solution to recombine
   * @return new solution produced by the crossover
   */
  def crossoverWith[T, G<:Solution](mate: T): G

  /**
   * The fitness is a value indicating how well a solution
   * does a determinated task
   * @return the fitness of this object
   */
  def getFitness(): Double

  /**
   * Compares two solutions based on their fitness
   * @param  that Solution to compare
   * @return 0 if both have same fitness
   *         1 if this object does it worse
   *         -1 if this object does it better
   *         By default, a solution is better than other if his fitness is lower
   */
  def compare (that: Solution): Int = {
    if (this == that) {
      return 0
    } else if (this.getFitness() <= that.getFitness()) {
      return -1
    } else {
      return 1
    }
  }
}
