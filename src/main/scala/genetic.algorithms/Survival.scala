package mx.unam.ciencias.hoc.gas.components

import scala.collection.mutable.MutableList
import scala.reflect.ClassTag
import PopulationOps.Population

/**
 * The way new solutions are introduced to next generation
 *
 * You should get the factory according the crossover factor you want to use
 * For example, to get 100% crossover you may use generational
 * if you want to add elitism, you can use steadyState(N-elitism)
 * if you want to crossover the 80% percent you can use steadyState(.8 * N)

 */
abstract class SurvivalMethod[A<:Solution]() {
  /**
   * Takes the new solutions and replace old solutions to get new population
   *
   * @return new population resulting of the selection
   */
  def survive(newIndividuals: MutableList[A], oldIndividuals: Population[A])(implicit m: ClassTag[A]): Population[A]
}

/**
 * Factory for survival methods
 */
object SurvivalMethodFactory {
  /**
   * Returns survival method based on steady-state strategy
   *
   * Takes the worst solutions of the population and replaces them with new ones
   * @param replaceN the number of solutions being droped of the population
   * @return a [[SurvivalMethod]] instance
   */
  def steadyState[A<:Solution](replaceN: Int): SurvivalMethod[A] = {
    class SteadyState[B<:Solution](newIntroduced: Int) extends SurvivalMethod[B] {
      def survive(newIndividuals: MutableList[B], oldIndividuals: Population[B])(implicit m: ClassTag[B]): Population[B] = {
        val survivors = PopulationOps.getTopN(oldIndividuals, oldIndividuals.length - newIntroduced)
        val introduced = newIndividuals.sortWith(_ < _)
        val new_population = new Population[B](oldIndividuals.length)

        for (index <- 0 until newIntroduced) {
          new_population(index) = introduced(index)
        }
        // add old
        var index = newIntroduced
        for (survivor <- survivors) {
          new_population(index) = survivor
          index += 1
        }

        new_population
      }
    }

    new SteadyState[A](replaceN)
  }

  /**
   * Returns survival method based on generational strategy
   *
   * Drop all old solutions and build new generation with new solutions
   * If the size of new solutions is lower then random solutions are added
   * @param factory the factory to create random solutions if needed
   * @return a [[SurvivalMethod]] instance
   */
  def generational[A<:Solution, F<:SolutionFactory[A]](factory: F): SurvivalMethod[A] = {
    class Generational[B<:Solution, G<:SolutionFactory[B]](factory: G) extends SurvivalMethod[B] {
      def survive(newIndividuals: MutableList[B], oldIndividuals: Population[B])(implicit m: ClassTag[B]): Population[B] = {
        val new_population = new Population[B](oldIndividuals.length)
        val extra = oldIndividuals.length - newIndividuals.length

        for (index <- extra until oldIndividuals.length) {
          val index_new = index - extra
          new_population(index) = newIndividuals(index_new)
        }

        if (extra > 0) {
          var index = 0
          while (index < extra) {
            val random = factory.getRandom()
            if (!new_population.contains(random)) {
              new_population(index) = random
              index += 1
            }
          }
        }

        new_population
      }
    }

    new Generational[A, F](factory)
  }
}
