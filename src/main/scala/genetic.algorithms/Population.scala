package mx.unam.ciencias.hoc
package gas
package components

import scala.reflect.ClassTag

/**
 * Manages Population operators
 */
object PopulationOps {
  type Population[T<:Solution] = Array[T]

  /**
   * Creates a new population of random individuals
   *
   * @param factory an object of [[SolutionFactory]] that implements random solutions
   * @return a new Population with random solutions
   */
  def createRandom[T<:Solution :ClassTag, F<:SolutionFactory[T]](factory: F, size: Int): Population[T] = {
    val population = new Array[T](size)
    var added = 0

    while(added < size) {
      val new_solution = factory.getRandom()
      if (!population.contains(new_solution)) {
        population(added) = new_solution
        added += 1
      }
    }

    population
  }

  /**
   * Get the best solutions of the population
   *
   * @param n the size of solutions taken
   * @return the n best solutions
   */
  def getTopN[T<:Solution](population: Population[T], n: Int): Array[T] = {
    val population_sorted = population.sortWith(_ < _) // give implicit ordering
    population_sorted.take(n)
  }


  /**
   * Find the best solution in the population
   *
   * This method should be used instead of getTopN(1)
   * for better complexity
   * @return fittest solution
   */
  def getBest[T<:Solution](population: Population[T]): T = {
    if (population.length == 0) {
      throw new NoSuchElementException("Population is empty")
    }

    var fittest = population(0)

    for (individual <- population) {
      if (individual < fittest) {
        fittest = individual
      }
    }

    fittest
  }
}
