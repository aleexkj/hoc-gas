import mx.unam.ciencias.hoc.gas.kct._
import mx.unam.ciencias.hoc.gas.utils.RNG
import org.scalatest._
import org.graphstream.graph._
import org.graphstream.graph.implementations._
import java.util.Iterator

class PrimTest extends FlatSpec with Matchers {

  val graph = GraphUtils.generateRandom(100)

  "generator" should "create k-card tree for given graph" in {
    RNG.setSeed(13)
    val node_init: Node = graph.getNode(RNG.chooseInt(100))
    val generator = new Prim(graph)
    generator.init(Left(node_init))
    val kct = new KCardinalityTree(f"${node_init.getId()}", 4)
    for (_ <- 1 to 4) {
      val edge = generator.next()
      kct.addFromEdge(edge)
    }

    assert(kct.nodes == 5)
    assert(kct.edges == 4)
    assert(kct.kCardinality == 4)
    assert(GraphUtils.isTree(kct))
  }

  it should "create k-card tree starting from an edge" in {
    val edge_init = GraphUtils.getMinEdge(graph) // get min edge from space
    val generator = new Prim(graph)
    generator.init(Right(edge_init))
    val kct = new KCardinalityTree(f"fromedge", 4)
    kct.addFromEdge(edge_init)

    for (_ <- 2 to 4) {
      val edge = generator.next()
      kct.addFromEdge(edge)
    }

    assert(kct.nodes == 5)
    assert(kct.edges == 4)
    assert(kct.kCardinality == 4)
    assert(GraphUtils.isTree(kct))
  }
}
