import mx.unam.ciencias.hoc.gas.kct._
import mx.unam.ciencias.hoc.gas.utils.RNG
import org.scalatest._
import org.graphstream.graph._
import org.graphstream.graph.implementations._
import java.util.Iterator

class KCardinalityTreeTest extends FlatSpec with Matchers {

  val graph = GraphUtils.generateRandom(100)
  val factory = new KCTFactory(graph, 10)
  val kct = factory.getRandom()

  "factory" should "create k-card tree for given graph" in {
    RNG.setSeed(13)

    assert(kct.nodes == 11)
    assert(kct.edges == 10)
    assert(kct.kCardinality == 10)

    var fitness = 0.0
    val iter_edges: Iterator[Edge] = kct.getEdgeIterator()
    while(iter_edges.hasNext()) {
      val edge: Edge = iter_edges.next()
      val weight: Double = edge.getAttribute("weight")
      fitness += weight
    }
    assert(100 + fitness - 1000 == kct.getFitness())
  }

  it should "create 2 random trees for given graph" in {
    RNG.setSeed(13)
    val pop = factory.generateRandom(2)

    assert(pop.size == 2)
    assert(pop(0) != pop(1))
    assert(pop(0).kCardinality == 10)
    assert(pop(1).kCardinality == 10)
  }

  "crossover" should "mix with tree" in {
    val m = factory.getRandom()
    val mate = GraphUtils.extendToContain(m, kct, graph)
    val offspring = kct.crossoverWith[Graph, KCardinalityTree](mate)

    assert(offspring.nodes == 11)
    assert(offspring.edges == 10)
    assert(offspring.kCardinality == 10)

    var fitness = 0.0
    val iter_edges: Iterator[Edge] = offspring.getEdgeIterator()
    while(iter_edges.hasNext()) {
      val edge: Edge = iter_edges.next()
      val weight: Double = edge.getAttribute("weight")
      fitness += weight
    }
    assert(100 + fitness - 1000 == offspring.getFitness())
    assert(GraphUtils.isTree(offspring))
  }

  it should "throw error with exclusive graph" in {
    assertThrows[Exception] {
      kct.crossoverWith[Graph, KCardinalityTree](new SingleGraph("empty"))
    }
  }

  it should "throw error with wrong instance" in {
    assertThrows[NotImplementedError] {
      kct.crossoverWith[MySolution, KCardinalityTree](new MySolution(2))
    }
  }

  "mutate" should "throw error" in {
    assertThrows[NotImplementedError] {
      kct.mutate(0.5)
    }
  }

  "equals" should "return true" in {
    assert(kct.equals(kct) == true)
  }

  it should "compare wiht other tree" in {
    val mate = factory.getRandom()
    val equals = mate.toString == kct.toString
    val requals = kct.equals(mate)
    assert(requals == equals)
  }

  it should "return false with other instance" in {
    assert(kct.equals(4) == false)
  }
}
