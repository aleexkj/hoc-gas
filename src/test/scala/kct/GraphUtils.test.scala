import mx.unam.ciencias.hoc.gas.kct._
import mx.unam.ciencias.hoc.gas.utils.RNG
import mx.unam.ciencias.hoc.gas.Constants._
import org.scalatest._
import org.graphstream.graph._
import org.graphstream.graph.implementations._
import java.util.Iterator

class GraphUtilsTest extends FlatSpec with Matchers {

  val graph = new SingleGraph("graph for test", false, true, 8, 9)
  var edge: Edge = graph.addEdge("13", "1", "3")
  edge.setAttribute("weight", 0.0.asInstanceOf[java.lang.Double])
  edge = graph.addEdge("23", "2", "3")
  edge.setAttribute("weight", 1.0.asInstanceOf[java.lang.Double])
  edge = graph.addEdge("34", "3", "4")
  edge.setAttribute("weight", 3.0.asInstanceOf[java.lang.Double])
  edge = graph.addEdge("48", "4", "8")
  edge.setAttribute("weight", 6.0.asInstanceOf[java.lang.Double])
  edge = graph.addEdge("38", "3", "8")
  edge.setAttribute("weight", 5.0.asInstanceOf[java.lang.Double])
  edge = graph.addEdge("36", "3", "6")
  edge.setAttribute("weight", 4.0.asInstanceOf[java.lang.Double])
  edge = graph.addEdge("37", "3", "7")
  edge.setAttribute("weight", 7.0.asInstanceOf[java.lang.Double])
  edge = graph.addEdge("78", "7", "8")
  edge.setAttribute("weight", 8.0.asInstanceOf[java.lang.Double])
  edge = graph.addEdge("85", "8", "5")
  edge.setAttribute("weight", 9.0.asInstanceOf[java.lang.Double])


  val graph2 = new SingleGraph("graph for test 2", false, true, 3, 2)
  edge = graph2.addEdge("12", "1", "2")
  edge.setAttribute("weight", 4.0.asInstanceOf[java.lang.Double])
  edge = graph2.addEdge("19", "1", "9")
  edge.setAttribute("weight", 10.0.asInstanceOf[java.lang.Double])

  val graph3 = new SingleGraph("graph for test 2", false, true, 4, 3)
  edge = graph3.addEdge("38", "3", "8")
  edge.setAttribute("weight", 5.0.asInstanceOf[java.lang.Double])
  edge = graph3.addEdge("36", "3", "6")
  edge.setAttribute("weight", 4.0.asInstanceOf[java.lang.Double])
  edge = graph3.addEdge("48", "4", "8")
  edge.setAttribute("weight", 6.0.asInstanceOf[java.lang.Double])

  "union" should "return graph1 U graph2" in {
    val g: Graph = GraphUtils.union(graph, graph2)
    assert(g.getNodeCount() == 9)
    assert(g.getEdgeCount() == 11)
  }

  "intersection" should "return empty in graph1 N graph2" in {
    val g: Graph = GraphUtils.intersect(graph, graph2)
    assert(g.getNodeCount() == 0)
    assert(g.getEdgeCount() == 0)
  }

  it should "return one edge of intersection" in {
    val graph3 = new SingleGraph("graph for test 3", false, true, 2, 1)
    edge = graph3.addEdge("13", "1", "3")
    edge.setAttribute("weight", 0.0.asInstanceOf[java.lang.Double])
    val g: Graph = GraphUtils.intersect(graph, graph3)
    assert(g.getNodeCount() == 2)
    assert(g.getEdgeCount() == 1)
  }

  "get min edge" should "return leatest weighted edge" in {
    var edge = GraphUtils.getMinEdge(graph3)
    assert(edge.getId() == "36")
  }

  it should "throw error without edges" in {
    assertThrows[Exception] {
      GraphUtils.getMinEdge(new SingleGraph("no edges"))
    }
  }

  "generate random" should "return graph by dorogotsev" in {
    val graph_random = GraphUtils.generateRandom(10)
    assert(graph_random.getNodeCount() == 10)
    val edge = graph_random.getEdge[Edge](0)
    assert(edge.hasAttribute("weight"))
    val weight: Double = edge.getAttribute("weight")
    assert(weight <= MAX_WEIGHT)
    assert(weight >= MIN_WEIGHT)
    // G.V * 3 - 6 = 24
    assert(graph_random.getEdgeCount() <= 24)
  }

  "read graph" should "return graph from test" in {
    val graph_read = GraphUtils.readGraph("src/test/test_read.dgs")
    assert(graph_read.getId() == "src/test/test_read")
    assert(graph_read.getNodeCount() == 10)
    assert(graph_read.getEdgeCount() == 17)
  }

  "extendToContain" should "return graph without changes" in {
    val graph_extended = GraphUtils.extendToContain(graph, graph3, graph)
    assert(graph_extended == graph)
  }

  it should "return graph modified so it contains other" in {
    val graph_extended = GraphUtils.extendToContain(graph2, graph3, graph)
    val intersection = GraphUtils.intersect(graph_extended, graph3)
    assert(intersection.getEdgeCount() > 0)
  }

  it should "throw error for impossible graphs" in {
    assertThrows[Exception] {
      GraphUtils.extendToContain(graph2, new SingleGraph(""), graph)
    }
  }

  "isTree" should "return false for not tree graph" in {
    assert(!GraphUtils.isTree(graph))
  }

  it should "return true for tree graph" in {
    assert(GraphUtils.isTree(graph2))
  }

  it should "return true for empty graph" in {
    assert(GraphUtils.isTree(new SingleGraph("empty")))
  }
}
