import org.scalatest._
import mx.unam.ciencias.hoc.gas.utils.io._
import mx.unam.ciencias.hoc.gas.utils.profile._

class UtilsTest extends FlatSpec with Matchers {

  "readAsOneString" should "read file and return string" in {
    val string = Files.readAsOneString("src/test/scala/input.example")
    assert(string == "abstractabcdefg")
  }

  "readSeparatedNumbers" should "return numbers in array" in {
    val array = Files.readSeparatedNumbers("src/test/scala/input_numbers.example")
    assert(array.length == 3)
    assert(array(0) == "1")
    assert(array(1) == "2")
    assert(array(2) == "3")
  }

  "time" should "profile method" in {
    val ellapsed = Timing.time {
      Thread.sleep(7)
    }

    assert(ellapsed >= 7 && ellapsed < 20) // concurrent
  }
}
