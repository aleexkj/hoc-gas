import org.scalatest._
import mx.unam.ciencias.hoc.gas.utils.RNG

class RNGTest extends FlatSpec with Matchers {
  RNG.setSeed(13)
  val numbers = Array[Int](1, 2, 3, 4, 5, 6)
  val probabilities = Array[Double](0.1, 0.1, 0.2, 0.2, 0.3, 0.1)

  "choice" should "return weighted element" in {
    RNG.setSeed(13)
    val result = RNG.choice[Int](numbers, probabilities)
    assert(result == 5)
  }

  it should "return first element" in {
    RNG.setSeed(13)
    val result = RNG.choice[Int](numbers, Array[Double](0.75, 0.05, 0.05, 0.05, 0.05, 0.05))
    assert(result == 1)
  }

  it should "return last element" in {
    RNG.setSeed(13)
    val result = RNG.choice[Int](numbers, Array[Double](0.05, 0.05, 0.05, 0.05, 0.05, 0.75))
    assert(result == 6)
  }

  it should "throw exception with no matching lengths" in {
    assertThrows[Exception] {
      RNG.choice[Int](Array[Int](1, 2, 3, 4), probabilities)
    }
  }

  it should "throw exception with wrong probabilities" in {
    assertThrows[Exception] {
      RNG.setSeed(13)
      RNG.choice[Int](numbers, Array[Double](0.1, 0.1, 0.1, 0.1, 0.1, 0.1))
    }
  }

  "shuffle" should "shuffle array in place" in {
    val numbers = Array[Int](10, 2, 4, 6, 9)
    RNG.shuffle[Int](numbers)
    val isTheSame = numbers(0) == 10 && numbers(1) == 2 && numbers(2) == 4 &&  numbers(3) == 6 && numbers(4) == 9
    assert(!isTheSame)
  }
}
