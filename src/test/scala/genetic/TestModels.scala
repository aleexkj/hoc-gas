import mx.unam.ciencias.hoc.gas.components.{PopulationOps, Solution, SolutionFactory}
import mx.unam.ciencias.hoc.gas.utils.RNG

class MySolution(value: Int) extends Solution {
  def mutate(p: Double) = {}

  def crossoverWith[G, T](mate: G): T = {
    mate.asInstanceOf[T]
  }

  def getFitness(): Double = {
    this.value
  }

  override def toString = this.value.toString

  override def equals(that: Any): Boolean = {
    if (that.isInstanceOf[MySolution]) {
      return this.value == that.asInstanceOf[MySolution].getFitness()
    }

    return false
  }
}

class MyFactory extends SolutionFactory[MySolution] {
  def getRandom(): MySolution = {
    val value = RNG.chooseInt(12)
    new MySolution(value)
  }
}
