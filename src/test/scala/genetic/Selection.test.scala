import org.scalatest._
import mx.unam.ciencias.hoc.gas.utils.RNG
import mx.unam.ciencias.hoc.gas.components._
import scala.collection.mutable.HashMap

class SelectionTest extends FlatSpec with Matchers {
  val pop_fixed = new Array[MySolution](4)
  pop_fixed(0) = new MySolution(10)
  pop_fixed(1) = new MySolution(2)
  pop_fixed(2) = new MySolution(4)
  pop_fixed(3) = new MySolution(9)

  val pop_fixed2 = new Array[MySolution](5)
  pop_fixed2(0) = new MySolution(10)
  pop_fixed2(1) = new MySolution(2)
  pop_fixed2(2) = new MySolution(4)
  pop_fixed2(3) = new MySolution(5)
  pop_fixed2(4) = new MySolution(1)

  "tournament" should "return tournament method implemented" in {
    var fittest = Array[Int](4, 9, 2, 10)
    RNG.setSeed(15)
    val mating_pool = SelectionMethods.tournamentSelection[MySolution](pop_fixed, 2)
    val set = mating_pool._1
    val prb = mating_pool._2
    var index = 0

    assert(set.length == 4)
    for (index <- 0 until 3) {
      assert(prb(index) == 1.0 / 4.0)
      fittest should contain (set(index).getFitness())
    }
  }

  it should "throw error with not enough individuals to compete" in {
    assertThrows[Exception] {
      SelectionMethods.tournamentSelection(pop_fixed, 6)
    }
  }

  "roulette" should "return proportional method implemented" in {
    RNG.setSeed(12)
    val mating_pool = SelectionMethods.proportionalSelection[MySolution](pop_fixed2)
    val set = mating_pool._1
    val prb = mating_pool._2
    assert(set.length == 5)
    val result = new HashMap[Int, Double]()
    // 4 5 1 10 2
    // 0.25 0.2 1 0.1 0.5 == 2.05
    // 0.1219 .0975 0.48 0.048 0.24
    // it works!
    result += 4 -> 0.25 / 2.05 // "0.12"
    result += 5 -> 0.2 / 2.05 // "0.10"
    result += 1 -> 1 / 2.05 // "0.49"
    result += 10 -> 0.1 / 2.05 // "0.05"
    result += 2 -> 0.5 / 2.05 // "0.24"
    // return shuffled
    val isTheSame = set(0).getFitness() == 10 &&
                    set(1).getFitness() == 2 &&
                    set(2).getFitness() == 4 &&
                    set(3).getFitness() == 5 &&
                    set(4).getFitness() == 1
    assert(!isTheSame)
    for (index <- 0 until 4) {
      assert(prb(index) == result(set(index).getFitness().toInt))
    }
  }
}
