import org.scalatest._
import mx.unam.ciencias.hoc.gas.utils.RNG
import mx.unam.ciencias.hoc.gas.components._
import scala.collection.mutable.MutableList

class SurvivalTest extends FlatSpec with Matchers {
  val pop_fixed = new Array[MySolution](5)
  pop_fixed(0) = new MySolution(10)
  pop_fixed(1) = new MySolution(2)
  pop_fixed(2) = new MySolution(4)
  pop_fixed(3) = new MySolution(6)
  pop_fixed(4) = new MySolution(9)

  val factory = new MyFactory()

  "generational" should "return new population of new individuals" in {
    val generational = SurvivalMethodFactory.generational[MySolution, MyFactory](factory)
    val newIndividuals = new MutableList[MySolution]()
    newIndividuals += new MySolution(11)
    newIndividuals += new MySolution(12)
    newIndividuals += new MySolution(13)
    newIndividuals += new MySolution(14)
    newIndividuals += new MySolution(15)
    val new_population = generational.survive(newIndividuals, pop_fixed)

    assert(new_population(0).getFitness() == 11)
    assert(new_population(1).getFitness() == 12)
    assert(new_population(2).getFitness() == 13)
    assert(new_population(3).getFitness() == 14)
    assert(new_population(4).getFitness() == 15)
  }

  it should "return generate random when new sols arent enough" in {
    val generational = SurvivalMethodFactory.generational[MySolution, MyFactory](factory)
    val newIndividuals = new MutableList[MySolution]()
    newIndividuals += new MySolution(13)
    newIndividuals += new MySolution(14)
    newIndividuals += new MySolution(15)
    val new_population = generational.survive(newIndividuals, pop_fixed)

    assert(new_population(0).getFitness() < 12)
    assert(new_population(1).getFitness() < 12) // factory creates with int below 12
    assert(new_population(2).getFitness() == 13)
    assert(new_population(3).getFitness() == 14)
    assert(new_population(4).getFitness() == 15)
    val old = Array[Int](13, 14, 15)
    assert(!old.contains(new_population(0).getFitness()))
    assert(!old.contains(new_population(1).getFitness()))
  }

  "steadyState" should "return new population with worst replaced" in {
    val steadyState = SurvivalMethodFactory.steadyState[MySolution](3)
    val newIndividuals = new MutableList[MySolution]()
    newIndividuals += new MySolution(11)
    newIndividuals += new MySolution(12)
    newIndividuals += new MySolution(13)
    newIndividuals += new MySolution(14)
    newIndividuals += new MySolution(15)
    val new_population = steadyState.survive(newIndividuals, pop_fixed)

    assert(new_population(0).getFitness() == 11)
    assert(new_population(1).getFitness() == 12)
    assert(new_population(2).getFitness() == 13) // 3 best from new
    assert(new_population(3).getFitness() == 2)
    assert(new_population(4).getFitness() == 4) // 2 best from old
  }
}
