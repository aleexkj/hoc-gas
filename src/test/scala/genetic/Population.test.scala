import org.scalatest._
import mx.unam.ciencias.hoc.gas.utils.RNG
import mx.unam.ciencias.hoc.gas.components._

class PopulationTest extends FlatSpec with Matchers {
  val pop_fixed = new Array[MySolution](5)
  pop_fixed(0) = new MySolution(10)
  pop_fixed(1) = new MySolution(2)
  pop_fixed(2) = new MySolution(4)
  pop_fixed(3) = new MySolution(6)
  pop_fixed(4) = new MySolution(9)

  val factory = new MyFactory()

  "createRandom" should "create random population with factory" in {
    val pop_gen = PopulationOps.createRandom[MySolution, MyFactory](factory, 2)
    assert(pop_gen.length == 2)
    assert(pop_gen(0) != pop_gen(1))
  }

  "getTopN" should "return best individuals of pop_fixed" in {
    val fittest = PopulationOps.getTopN(pop_fixed, 2)
    assert(fittest.length == 2)
    assert(fittest(0).getFitness() == 2)
    assert(fittest(1).getFitness() == 4)
  }

  "getBest" should "return best individual of pop_fixed" in {
    val fittest = PopulationOps.getBest(pop_fixed)
    assert(fittest == pop_fixed(1))
  }

  it should "throws exception for population empty" in {
    assertThrows[Exception] {
      PopulationOps.getBest(new Array[MySolution](0))
    }
  }
}
