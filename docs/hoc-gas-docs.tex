\documentclass[9pt,twocolumn,twoside,spanish]{optica}
\setboolean{shortarticle}{true}
\setboolean{minireview}{false}

\usepackage{dirtytalk}
\usepackage{epigraph}
\usepackage{hyperref}

\title{Algoritmos Genéticos y el problema KCT}
\author[1]{Alejandra Coloapa Díaz}
\affil[1]{aleexkj@ciencias.unam.mx}
\setboolean{displaycopyright}{false}

\begin{document}
\begin{abstract}
Los algoritmos genéticos (AGs) son una herramienta de búsqueda y optimización, han sido usados ampliamente para optimizar problemas en diversas áreas como ciencia, comercio e ingeniería. Su éxito se debe a su amplia aplicabilidad, facilidad de uso y perspectiva global\citep{Goldberg:1989}.

Usando algoritmos genéticos se pueden aproximar soluciones a problemas NP-duros de optimización combinatoria, en particular, el problema KCT\footnote{generaliza el árbol generador de peso mínimo}.\end{abstract}
\maketitle

\section{Introducción}

\epigraph{\say{survival of the fittest will win}}{}

Los algoritmos genéticos (AGs) fueron creados para imitar algunos de los procesos observados en la evolución natural. Mucha gente, biólogos incluidos, está asombrada de que la vida - en el nivel de complejidad que observamos - pudo haber evolucionado en relativamente poco tiempo. La idea con los AGs es usar el poder de la evolución para resolver problemas de optimización.

Los AGs se caracterizan principalmente por trabajar con poblaciones de individuos, aplicando operadores de \textit{recombinación} y \textit{mutación}. La parte esencial de un AG es la \textit{selección} de individuos según su \textit{rendimiento}, donde los individuos mejor adaptados son más propensos a pasar a las siguientes iteraciones o ser elegidos como padres de nuevos individuos. Este principio es llamado \textit{supervivencia del más apto} (\textit{survival of the fittest}) en la evolución natural. El padre del algoritmo original es John Holland que lo inventó a inicios de 1970.
\section{Componentes}
\label{sec:components}

\subsection{Individuos}
Un individuo es una solución potencial a un problema. Se considera como elemento de una colección finita $\mathrm{R}$ dentro del espacio de soluciones $\Omega$.

Una solución es codificada en un \textit{cromosoma} sobre un alfabeto $\mathrm{A}$ (en general binario). Un \textit{cromosoma} tiene longitud fija.

\begin{figure}[htbp]
\centering
\fbox{\includegraphics[scale=0.6]{chromosome.png}}
\caption{Una solución (cilindro) y su cromosoma codificado como la cadena \texttt{"d h"} en binario. Ejemplo tomado de \cite{cans:example}}
\label{fig:chromosome}
\end{figure}

\subsection{Población}
Una población es una colección finita $R$ de soluciones.

En cada iteración del algoritmo la población es modificada eliminando individuos y agregando nuevos para crear una nueva población, manteniendo un tamaño fijo entre cada iteración.

\section{Función de Aptitud}
Calcula la \textit{aptitud} de un individuo en su ambiente (evalúa qué tan buena es una solución).
Si el objetivo del problema es minimizar cierto valor entonces la función de aptitud evaluará a menores valores de aptitud para mejores soluciones y visceversa.

Considerando el ejemplo de la Fig.\ref{fig:chromosome} (donde se busca reducir el material empleado para construir un cilindro de cierta capacidad), la función de aptitud (material empleado) devolverá menores valores para cilindros que usen menos material y mayor para cilindros que lo desperdicien.

La función de aptitud $f$ toma como entrada un cromosoma y evalúa su rendimiento, éste valor puede variar dependiendo del problema. Para maneter uniformidad se usan técnicas para mapear el costo objetivo a un valor de aptitud. Dos técnicas comunes para mapear dichos valores son la siguientes:

\begin{enumerate}
\item $Windowing$. Sea $v_w$ el costo del peor cromosoma en la población, entonces para cada cromosoma $i$ su valor de aptitud $f(i)$ es proporcional a la diferencia de costo entre él y el peor cromosoma:
\begin{align}
\label{eq:windowing_fitness}
f(i) = c \pm (v_i - v_w)
\end{align}
donde $v_i$ es el costo del cromosoma $i$, $c$ es una constante predefinida y se usa una suma o resta dependiendo el comportamiento que se busca en el valor de aptitud.
\item  Normalización lineal. Los cromosomas se ordenan descendentemente o ascendentemente por su costo dependiendo si se quiere maximizar o minimizar. Se define $f_{best}$ como el mejor valor de aptitud y la función de aptitud del $i-$ésimo cromosoma:
\begin{align}
\label{eq:linal_fitness}
f(i) = f_{best} - (i-1) * d
\end{align}
donde $d$ es un factor de decremento. De esta forma el valor objetivo promedio se mapea al valor de aptitud promedio.
\end{enumerate}

\section{Operadores Genéticos}
\subsection{Recombinación}
Operador que combina subpartes de dos cromosomas padres para crear un hijo que contenga material genético de ambos. Se considera un factor que distingue los AGs de otros algoritmos de optimización.

Para recombinar es necesario seleccionar a los padres primero, emulando la supervivencia del más apto. Se espera que los cromosomas con mejor aptitud se recombinen con mayor probabilidad y pasen a la siguiente iteración.

\subsubsection{Selección}
\label{sec:selection_strategy}
Existen muchas formas de seleccionar a los padres (crear \textit{Mating Pool}), entre ellas:

\begin{table}[htbp]
\centering
\caption{\bf Valores de aptitud muestra}
\begin{tabular}{ccc}
\hline
\# & valor de aptitud \\
\hline
Cromosoma 1 & 15 \\
Cromosoma 2 & 27 \\
Cromosoma 3 & 6 \\
Cromosoma 4 & 52 \\
Cromosoma 5 & 11 \\
\hline
\end{tabular}
  \label{tab:original_example_fitness}
\end{table}

\begin{table}[htbp]
\centering
\caption{\bf Valores de aptitud para Ranking}
\begin{tabular}{ccc}
\hline
\# & valor de aptitud \\
\hline
Cromosoma 1 & 3 \\
Cromosoma 2 & 4 \\
Cromosoma 3 & 1 \\
Cromosoma 4 & 5 \\
Cromosoma 5 & 2 \\
\hline
\end{tabular}
  \label{tab:ranking_fitness}
\end{table}

\begin{enumerate}
\item Ranking. Se ordenan los cromosomas acorde a su valor de aptitud y se define un nuevo valor de aptitud: el cromosoma con el mayor/menor recibe $N$, el siguiente $N-1$ hasta que el peor cromosoma obtenga valor de aptitud 1. Con esta re-asignación de valores de aptitud se sigue la selección proporcional. El Cuadro \ref{tab:ranking_fitness} muestra un ejemplo de los valores de aptitud obtenidos a partir del Cuadro \ref{tab:original_example_fitness}.
\item Proporcional. Es el método original propuesto por Holland. A cada cromosoma se le asigna una probabilidad de ser elegido en proporción a su fitness:
\begin{align}
\label{eq:proportional_selection}
p_i = \frac{f(i)}{\Sigma_{j\in\Omega} f(j)}
\end{align}
donde $f(i)$ es el valor de aptitud del cromosoma $i$ y $\Omega$ es la población. Esta estrategia se ve afectada por la distribución de la función de aptitud, pudiendo ocasionar un sesgo.
\item Torneo. En lugar de usar probabilidades para seleccionar los padres se hace un torneo basado en el valor de aptitud de los cromosomas. Se toman $t$ elementos al azar de la población y se toma el mejor de ellos. Los padres se seleccionan de forma aleatoria dentro de los ganadores. En esta estrategia se espera que las mejores soluciones ganen hasta dos veces y las peores ninguna vez.
\end{enumerate}

\begin{figure}[htbp]
\centering
\fbox{\includegraphics[scale=0.2]{ranking_selection.png}}
\caption{Distribución en la selección por ranking}
\fbox{\includegraphics[scale=0.2]{proportional_selection.png}}
\caption{Distribución en la selección proporcional}
\fbox{\includegraphics[scale=0.8]{tournament_selection.png}}
\caption{\textit{Mating Pool} resultante con los ganadores del torneo con $t = 2$}
\label{fig:selection_methods}
\end{figure}

\subsubsection{Reproducción}
Una vez seleccionados los padres, se combinan y se obtienen dos nuevos cromosomas. Existe gran número de variaciones para esta operación, siendo la más común la recombinación en un punto. La Fig.\ref{fig:crossover} muestra ejemplos para distintas variaciones.

\begin{figure}[htbp]
\centering
\fbox{\includegraphics[scale=0.25]{crossover_examples.png}}
\caption{(a) Simple. Se selecciona un punto aleatorio y se parte el cromosomoa de los padres para formar dos hijos con distintas secciones. (b) Múltiples puntos. Se seleccionan diversos puntos de forma aleatoria y se sigue la misma idea. (c) Uniforme. Se crea una máscara y se toma un gen de un cromosoma del primer padre si es \texttt{1} o del segundo padre si es \texttt{0}, se realiza para todos las posiciones.}
\label{fig:crossover}
\end{figure}

\subsection{Mutación}
Es un operador que introduce variaciones en un cromosoma de forma aleatoria según una probabilidad.
Este operador mantiene la diversidad de la población y su objetivo es encontrar una mejor solución.

En la forma tradicional un gen del cromosoma es cambiado si se pasa un test de probabilidad, aunque existe la posibilidad de cambiar más de un gen.

\begin{figure}[ht]
\centering
\fbox{\includegraphics[scale=0.25]{mutation_examples.png}}
\caption{Mutación puntual y múltiple.}
\label{fig:mutation}
\end{figure}

\section{Reemplazo}
\label{sec:replace}
Después de generar un conjunto nuevo de individuos se pueden seguir dos estrategias de reemplazo:

\begin{enumerate}
\item Generacional. Cada población de tamaño $n$ genera el mismo número de cromosomas nuevos que reemplazan la población por completo. Sin embargo, esto puede ocasionar que los mejores miembros de la población no generen mejores en la siguiente iteración por lo que éste método suele ser combinado con una estrategia de \textit{elitismo} donde uno o más cromosomas se copian intactos en la nueva población.
La estrategia de \textit{elitismo} puede hacer que un cromosoma sea dominante en todas las iteraciones pero a su vez puede mejorar el rendimiento.
\item Steady-state. Sólo unos cromosomas son reemplazados para generar la siguiente población. Usualmente se reemplaza a los peores por nuevos cromosomas. La cantidad de cromosomas generados necesita ser predefinida, en práctica sólo uno o dos cromosomas nuevos se introducen.
\end{enumerate}

\section{Evolución}

El operador de recombinación tiende a seleccionar los mejores cromosomas y combinarlos con fin de obtener mejores soluciones, mientras que el operador de mutación altera localmente para mejorar el resultado.

Gracias a esto las poblaciones mejoran en cada iteración pues hay una alta probabilidad de supervivencia de los mejores y se espera que las soluciones \textit{malas} sean descartadas posteriormente.

Por otra parte, una mala configuración de los operadores puede resultar en una búsqueda inútil. Por ejemplo, una alta recombinación puede deformar buenos cromosomas mientras que la mutación puede convertir la heurística en una búsqueda aleatoria.

\section{Algoritmo Genético}

En general un algoritmo genético tiene la estructura descrita en \ref{alg:gas}.

\begin{algorithm}[ht]
\caption{Algoritmo Genético}\label{alg:gas}
\begin{algorithmic}[1]
\Procedure{AG}{$n, Q$}\Comment{Población de tamaño $n$ y condición de paro $Q$}
\State $\Omega\gets SolucionesAleatorias(n)$
\State $g\gets 1$
\While{\textbf{not} $Q$}
\State $\Lambda\gets$ Seleccion($poblacion$)
\State $\Upsilon\gets \emptyset$
\While{$\|\Upsilon\| < c * \|\Omega\|$}
\State Sean $p1, p2\in\Lambda$
\State $h1, h2\gets$Recombina($p1, p2$)
\State $h1\gets$Mutacion($h1$)
\State $h2\gets$Mutacion($h2$)
\State $\Upsilon \gets \Upsilon \cup \{h1, h2\}$
\EndWhile
\State $\Omega\gets$ Reemplazo($\Omega, \Upsilon$)
\State $g\gets g + 1$
\EndWhile
\State \textbf{return} Mejor($\Omega$)
\EndProcedure
\end{algorithmic}
\end{algorithm}

Se generan $n$ soluciones aleatorias que forman la población inicial (generación $g = 1$). Mientras la condición de paro no se cumpla:
\begin{itemize}
\item Se selecciona el conjunto de cromosomas padres $\Lambda$ de acuerdo a una estrategia de selección (ver secc. \ref{sec:selection_strategy}).
\item Se construye una nueva población $\Upsilon$ de acuerdo a $c$ el factor de recombinación, es decir, dependiendo de la estrategia de reemplazo (ver secc. \ref{sec:replace})
\item Para recombinar se seleccionan dos cromosomas de forma aleatoria en $\Lambda$ y se aplica el operador de combinación; enseguida se mutan los cromosomas resultantes y se agregan a $\Upsilon$
\item Los nuevos individuos se introducen y forman una nueva población que será usada en la siguiente generación.
\end{itemize}

Cuando la condición $Q$ se cumpla se devuelve el mejor individuo obtenido.

\subsection{Condiciones de paro}
\begin{itemize}
\item Número de generaciones predefinido
\item Variación mínima entre dos generaciones
\item Valor de aptitud predefinido
\end{itemize}

\subsection{Parámetros}
\begin{itemize}
\item 60 - 100\% de la población debe ser introducida en cada generación, aumenta si la población es pequeña.
\item 0.5 - 1\% del material genético de un cromosoma es mutado, si la población es grande esta probabilidad decrece.
\item 50 - 200 cromosomas por generación
\item Usualmente sólo un cromosoma es pasado por elitismo
\end{itemize}

\section{El problema KCT}
Dada una gráfica $G =(V, E)$, el problema KCT\footnote{$k$-cardinality tree} consiste en encontrar un árbol $T\subset G$ con exactamente $k\leq |V| - 1$ aristas tal que la suma de sus pesos es mínimo. Este problema ha ganado interés considerable debido a sus aplicaciones en campos de petróleo, descomposición de matrices y telecomunicaciones\citep{kct1}.

Formalmente puede ser definido como sigue:

Sea $G = (V, E)$\footnote{$E(G), V(G)$ denota las aristas y vértices en $G$ respectivamente} una gráfica con una función de peso
\begin{align*}
w: E \rightarrow \mathrm{R}^+
\end{align*} en las aristas. Sea $\mathcal{T}_k$ el conjunto de todos los árboles de cardinalidad $k$ en $G$. El problema KCT $(G, w, k)$ consiste en encontrar el árbol $T_k\in\mathcal{T}_k$ que minimice
\begin{align}
f(T_k) = \Sigma_{e\in E(T_k)}w(e)
\end{align}

Si $k = |V| - 1$ se convierte en el problema del árbol generador de peso mínimo que puede resolverse en tiempo polinomial. Por otra parte, para $k = 1, 2$ es fácil pues basta encontrar la arista de peso mínimo o una búsqueda en $O(n^2)$ para dos aristas conectadas.

\subsection{Espacio de búsqueda}
Sea $G$ la gráfica de entrada y $T_k$ un árbol de cardinalidad $k$ en $G$, los vecinos de $T_k$ consisten en todos los árboles de cardinalidad $k$ que pueden ser generados eliminando una arista $e\in E(T_k)$ que resulte en un árbol $T_{k-1}\in G$ y agregando una arista del conjunto $NH(T_{k-1}) \setminus \{e\}$, donde
\begin{align}
\label{eq:neighbor}
NH(T) = \{e = (v, v')\in E(G) | v\in V(T) \oplus v'\in V(T)\}
\end{align}

i.e $NH$ es el conjunto de todas las aristas en $G$ que no pertenecen a $T$ y que tienen exactamente un vértice en $T$.

Agregar una arista de $NH$ garantiza que la estructura permanece siendo un árbol pues sigue siendo conexa (para llegar al nuevo vértice $v'$ basta encontrar el camino a $v$ y unir la arista $e$) y no tiene ciclos (se agrega una arista con un vértice ajeno al árbol).

\subsection{Construcción de árboles}
Un árbol puede ser construido a partir de un vértice o una arista en $G$. Se sigue el algoritmo de Prim con el espacio de búsqueda descrito anteriormente. Para introducir variación, en cada paso de Prim se agrega la arista mínima bajo un criterio de probabilidad\citep{kct2}, e.o.c se considera la segunda menor arista y así sucesivamente.

\subsection{Algoritmo Genético}
Dado un problema KCT $(G, w, k)$ se definen los componentes del algoritmo genético como sigue:

\begin{itemize}
\item[\bf A] Un individuo es un árbol de cardinalidad $k$ en $G$ cuya función de aptitud es la suma de los pesos de sus aristas, por lo que se busca minimizar dicha función. Se usa la técnica de $windowing$:
\begin{align*}
f(T_k) = C + (f_{T_k} - k * MAX\_WEIGHT)
\end{align*}
donde $f_{T_k}$ es la suma de los pesos de las aristas y $C, MAX\_WEIGHT$ son constantes predefinidas. De esta forma, el valor máximo de la función de aptitud es $C$ y el valor mínimo es $C + k * MIN\_WEIGHT - k * MAX\_WEIGHT$.
\item[\bf B] La población tiene tamaño $\lfloor \frac{|E(G)|}{k} \rfloor$ con un mínimo de 50 individuos y máximo 200. Cada población tiene individuos únicos.
\item[\bf C] Se seleccionan dos árboles $T_k \neq T_k^p$ tal que $T_k \cap T_k^p \neq \emptyset$, de no ser así se extiende un $T_k^p$ hasta que la intersección contenga una arista. Para combinarlos se construye un árbol a partir de la arista de menor peso en $T_k \cup T_k^p$.
\begin{algorithm}
\begin{algorithmic}[1]
\Procedure{AGRecombine}{$T_k, T_k^p$}
\State $E_{\cup}\gets E(T_k) \cup E(T_k^p)$
\State $E(T^c)\gets \min\{w(e=(v,v')) | e\in E_{\cup}\}$
\State $V(T^c)\gets \{v, v'\}$
\While{$|E(T^c)| < k$}
\State $e\gets \min \{d \in E(NH(T^c) \cap E_\cup)\}$
\While{ \textbf{not} Criterio de probabilidad acepta $e$}
	\State $e \gets \min\{d \in E(NH(T^c) \cap E_\cup) | d < e\}$
\EndWhile
\State $E(T^c) \gets E(T^c)\cup\{e\}$
\State $V(T^c)\gets V(T^c)\cup\{v, v'\}$
\EndWhile
\State \textbf{return} $T^c$
\EndProcedure
\end{algorithmic}
\caption{Recombinación. Las aristas con peso menor son preferidas bajo una cierta probabilidad; de otra forma se sigue con la segunda mejor arista, etc. Esto garantiza que la selección de aristas no sea determinista, dando aleatoridad a la construcción e introduciendo variedad cada que dos árboles se recombinan.}\label{alg:gas_crossover}
\end{algorithm}
\item[\bf D] No se implementa mutación. Para compensar la introducción de nuevo material genético, se agrega un factor de edad a los árboles. Cada que se genera un árbol nuevo éste tiene edad 0 y por cada generación que sobrevive, aumenta en 1 su edad.
\item[\bf E] Se usa selección proporcional pues se busca que los árboles se combinen con cualquiera (extiendan su visión de la gráfica) y no se combinen sólo con los mejores (o la población se aislaría a una zona de la gráfica).
\item[\bf F] Se usa reemplazo generacional, sin embargo, al tener un factor de edad y unicidad de individuos en las poblaciones, puede que en alguna generación se produzcan menos individuos por lo que se introducen nuevos generados aleatoriamente. Por otra parte, se agrega un individuo resultante de recombinación solamente si es mejor a su árbol padre.
\item[\bf G] Una población evolucionará hasta encontrar una mejor solución. En cada generación se introduce material aleatorio para conservar soluciones sobre toda la gráfica, por lo que una población no necesariamente sólo mejora (puede que las soluciones introducidas sean muy malas). Además, al agregar un factor de edad las poblaciones tienden a seguir cambiando y no converger. La evolución se da por etapas: una población llega a un mínimo y si no puede encontrar otro (mismas soluciones pasan a la siguiente generación) eventualmente se descarta y se genera otra población que busque otro mínimo.
\end{itemize}

El algoritmo genético resultante:
\begin{algorithm}
\caption{Algoritmo para el problema KCT}\label{alg:gas_kct}
\begin{algorithmic}[1]
\Procedure{AG-KCT}{$G, w, k$}
\State $T^g\gets NULL$
\State $n\gets$DeterminaN$(G, k)$
\State $\Omega\gets SolucionesAleatorias(n)$
\While{\textbf{not} $Q$}
\State $\Lambda\gets$ Recombinacion($\Omega$)
\State $\Lambda\gets $ EliminaViejos($\Lambda$)
\State $\Omega\gets$ Reemplazo($\Omega, \Lambda$)
\State $T^{i}\gets min\{f(T_k) | T_k\in\Omega\}$
\State EligeMejor$(T^g, T^i)$
\EndWhile
\State \textbf{return} $T^g$
\EndProcedure
\end{algorithmic}
\end{algorithm}

donde $T^g$ es la mejor solución global y $T^i$ la mejor de cada iteración.

\textsf{DeterminaN} obtiene el tamaño de la población como fue descrito. Si el tamaño es mayor al número de vértices en $G$, se acota a $|V(G)|$.

\textsf{SolucionesAleatorias} genera $n$ árboles de cardinalidad $k$ de forma aleatoria, cada uno a partir de un vértice en $V(G)$.

\textsf{Recombinacion} Se crea la \textit{Mating Pool} usando selección proporcional. El 100\% de los individuos es recombinado: por cada árbol $T_k$ se selecciona otro en \textit{Mating Pool} y se recombinan como fue descrito. Si el árbol resultante ya pertenece a la población actual se incrementa la edad y se agrega a la nueva población; si el árbol resultante es un árbol nuevo que es mejor a $T_k$ se agrega a la nueva población; si el árbol resultante es peor que $T_k$ se incrementa la edad de $T_k$ y sobrevive a la siguiente generación.

\textsf{EliminaViejos} Elimina todos los árboles en la nueva población cuya edad es mayor a 10.

\textsf{Reemplazo} Agrega todos los individuos generados a la nueva población, si $|\Lambda| < n$ se introducen nuevos árboles a la población para mantenerla de tamaño fijo. En cada reemplazo de población se garantiza la unicidad de los individuos.

\textsf{EligeMejor}. Actualiza la mejor solución global con la obtenida en la iteración.

\subsection{Parámetros}
\subsubsection{Cardinalidad}
Se mantiene una población que cubra todas las aristas del árbol, pero manteniendo cotas para una ejecución estable (si se genera una población de 10000 individuos, esto sería muuy lento). Se acota a su vez al número de nodos, pues se genera un árbol por nodo y con esto se cubren todas las aristas $buenas$ ya que Prim tomará lo mejor que haya disponible.
\subsubsection{Probabilidad}
Se agregó un criterio de probabilidad a Prim para dejarle tomar aristas $malas$ de vez en cuando, esto para buscar más alternativas que pudieran llevar a una mejor solución.
Se elige una probabilidad entre $[0.5, 0.85]$ en la construcción de cada árbol.

Las figuras \ref{fig:prim_proba_a} - \ref{fig:prim_proba} muestran experimentación con distintas probabilidades para un problema con 150 vértices, 297 aristas y cardinalidad 15. Las gráficas muestran la generación y la mejor solución encontrada.
\begin{figure}[htbp]
\centering
\fbox{\includegraphics[scale=0.5]{20_percent_prim.png}}
\caption{Probabilidad 0.2. Mejor solución 129.76}
\label{fig:prim_proba_a}
\fbox{\includegraphics[scale=0.5]{50_percent_prim.png}}
\caption{Probabilidad 0.5. Mejor solución 119.215}
\end{figure}
\begin{figure}[htbp]
\fbox{\includegraphics[scale=0.5]{85_percent_prim.png}}
\caption{Probabilidad 0.85. Mejor solución 119.215}
\fbox{\includegraphics[scale=0.5]{100_percent_prim.png}}
\caption{Probabilidad 1.0 Mejor solución 128.13}
\label{fig:prim_proba}
\end{figure}

\subsubsection{Edad}
Sin meter un factor de edad (al reemplazar un árbol sólo si se encuentra uno mejor como su hijo) la población converge muy rápido, pues las soluciones sobreviven por más tiempo. Después de experimentación, si no se incluye el factor de edad ni unicidad, la evolución es notoria:
\begin{figure}[H]
\fbox{\includegraphics[scale=0.5]{no_changes.png}}
\caption{Población sin unicidad ni factor de edad. Mejor solución 263}
\end{figure}
Sin embargo, para ejemplos grandes no siempre encuentra la mejor solución.
Al introducir unicidad en los individuos de la población, la variación aumenta por los individuos aleatorios introducidos:
\begin{figure}[H]
\fbox{\includegraphics[scale=0.5]{ind_unique.png}}
\caption{Población con unicidad sin factor de edad. Mejor solución 263}
\end{figure}
sin embargo, se nota como decrece la mejor solución.
Introduciendo el factor de edad, puede que poblaciones casi completas se generen en alguna generación (y esto causa ruido en la gráfica):
\begin{figure}[H]
\fbox{\includegraphics[scale=0.5]{age_factor.png}}
\caption{Población con unicidad y factor de edad. Mejor solución 236}
\end{figure}
Es más ligero el comportamiento decreciente de la función de aptitud, sin embargo, introducir material nuevo ayuda en la búsqueda de soluciones pues en este caso se encontró una solución mejor que con las configuraciones anteriores.

Al decidir a qué edad se descartan soluciones, en gráficas pequeñas no hay cambios mientras que en gráficas grandes hay variación. Siguiendo la literatura dejé el parámetro en 10 (y funciona bien).

\subsubsection{Condición de paro}
Seguido de la introducción de nuevo material (cuando individuos son eliminados por la edad), si una nueva población vuelve a converger a la misma solución global se toma como indicio que debe parar. La condición de paro es entonces: el número de veces que la solución global es la misma sea menor a 3 veces el factor de edad. Es decir, contando cuántas generaciones la solución local es igual a la solución global y comparando contra el factor de edad. La mejor solución estará viva a lo más 10 generaciones.. si después de introducir nuevo material se vuelve a encontrar la misma y no mejora, estará otras 10 veces y así sucesivamente. El algoritmo para cuando esto pasa una tercera vez porque \textit{la tercera es la vencida}.

\begin{figure}[ht]
\centering
\fbox{\includegraphics[scale=0.5]{stop_condition.png}}
\caption{Comportamiento con 1000 nodos y cardinalidad 15. La mejor solución está viva por un tiempo, se introduce material y se vuelve a ella.}
\label{fig:mutation}
\end{figure}

\section{Implementación}
\begin{itemize}
\item Los detalles sobre selección y supervivencia se implementan seguido de las definiciones.

\item Para definir un árbol se extiende una gráfica para manejar el $fitness$ y métodos necesarios como comparación, igualdad y combinación.

\item Se separan todas las constantes de configuración, dejando a experimentación los valores seleccionados.

\item Se implementa una clase estática que maneje operadores comunes a una población y otra que maneje operaciones sobre gráficas.

\item Se implementa una interfaz y la clase para árboles que tiene el comportamiento para una solución (individuo) en un algoritmo genético.

\item La construcción de árboles se hace en otra clase, siguiendo el comportamiento de un $Generador$.

\item El algoritmo genético se implementa seguido de la descripción dada y los métodos definidos en los diversos componentes.

\item Se usa la biblioteca \textbf{GraphStream} para el manejo de las gráficas, aprovechando que ya cuenta con una implementación para GUI. La clase $KCTViewer$ contiene métodos para dar formato a una gráfica (que representan cambios en la GUI). Una gráfica de \textbf{GraphStream} se guarda en un archivo $.dgs$ que puede ser usado como entrada al problema.
\end{itemize}

Ver \href{https://drive.google.com/a/ciencias.unam.mx/file/d/0B5asSXuU1mTsMnhqM0JxdnZ4NG8/view?usp=sharing}{diagrama UML}

\subsection{Integraciones}
El proyecto está alojado en \href{https://gitlab.com/aleexkj/hoc-gas}{GitLab}. Cuenta con integración a \href{https://gitlab.com/aleexkj/hoc-gas/blob/master/.gitlab-ci.yml}{GitLab CI} para correr pruebas unitarias, actualizar la documentación y en caso de ramas, verificar la compilación al momento de subir cambios. La documentación de las clases se encuentra \href{https://aleexkj.gitlab.io/hoc-gas/}{aquí}.

Tiene integración \href{https://www.docker.com/}{\bf Docker} para ejecutar el proyecto. Para ejecutar con GUI es necesario correr desde CLI (se puede hacer a través de docker pero requiere más configs). El README del proyecto contiene las versiones de Scala y SBT necesarias, así como instrucciones para ejecutar el proyecto.

\subsection{Ejecución}
\begin{verbatim}
Usage: k_cardinality [options] <seed> <k_cardinality>

   --help                   Show this help text
   --version                Show version
   -g, --gui                Wether to show GUI or not
   -l, --labels             Wether to show labels
                             for weights or not
   <seed>                   RNG seed
   <k_cardinality>          Cardinality of tree
   							to look for
   -i, --input <dgs-file-path>
                            DGS file to load graph
   -o, --output <file-prefix>
                            If passed saves the k-card
                            	tree found and
                            	the graph source
   -n, --nodes <nodes-graph>
                            Indicates how many nodes are
                            	in the graph source generated.
                            	(Default: 100)

\end{verbatim}

El proyecto requiere al menos dos parámetros: la semilla y la cardinalidad del árbol a buscar. Se puede pasar el número de nodos de la gráfica universo o una gráfica como entrada. Se puede especificar el prefijo de los archivos donde se guardarán la gráfica y solución obtenida: $prefijo$\_graph.dgs, $prefijo$\_solution.dgs respectivamente. Si se muestra GUI se puede pasar la opción \texttt{-l} para mostrar los pesos en las aristas.

Durante ejecución se muestra el fitness de la mejor solución encontrada hasta el momento y la solución final. Se guarda en un archivo con sufijo $\_output$ el promedio del fitness de la población en cada generación y el fitness de la mejor solución en cada iteración. Dicho archivo puede ser graficado con: 

\texttt{scripts/plot.sh [file]}

% Bibliography
\bibliography{sample}
\bibliographyfullrefs{sample}

\renewcommand{\refname}{Bibliografía}
\begin{thebibliography}{1}
\bibitem{theory}
K. S. Tang, K. F Man, S. Kwog and Q. He. Genetic Algorithms and their Applications. \textit{IEEE Signal Processing Magazine}. 13:22-37, 1996

Thiele, Lothar and Blickle, Tobias. A Comparision of Selection Schemes used in Genetic Algorithms. \textit{Swiss Federal Institue of Technology}. 1995.
\end{thebibliography}

\end{document}
