**HOC - 2017-1 - FC UNAM**

[![scala](https://img.shields.io/badge/scala-v2.11.8-red.svg)](https://www.scala-lang.org/)
[![sbt](https://img.shields.io/badge/sbt-v0.13.12-blue.svg)](http://www.scala-sbt.org/)
[![build status](https://gitlab.com/aleexkj/hoc-gas/badges/master/build.svg)](https://gitlab.com/aleexkj/hoc-gas/commits/master)
[![coverage report](https://gitlab.com/aleexkj/hoc-gas/badges/master/coverage.svg)](https://gitlab.com/aleexkj/hoc-gas/commits/master)
[![api docs](https://img.shields.io/badge/docs-99%25-yellowgreen.svg)](https://aleexkj.gitlab.io/hoc-gas/)
# Usage
With SBT and Scala installed:
- ``$ sbt update`` to download scala dependencies
- ``$ sbt compile pack && cd dist/`` to make package
- then ``$ bin/kct_genetic_algorithm --help`` (or ``make install`` to get bin installed)
- ``$ sbt clean coverage test coverageReport`` to run the (no-deterministic) tests and get the coverage report under ``target/scala-2.11/scoverage-report``
With Docker:
- ``$ docker run registry.gitlab.com/aleexkj/hoc-gas:latest --help``

## Documentation
- See complete [article](https://gitlab.com/aleexkj/hoc-gas/tree/master/docs/hoc-gas-docs.pdf) to get more information about ``Genetic Algorithms`` and the problem solved.
- Look at the [Wiki](https://gitlab.com/aleexkj/hoc-gas/wikis/home) for quick overview.

[![complex code, obvs](http://forthebadge.com/images/badges/just-plain-nasty.svg)](https://cdn.meme.am/instances/500x/22958620.jpg)
[![cats<3](http://forthebadge.com/images/badges/contains-cat-gifs.svg)](https://media.giphy.com/media/zplnex6NtT6F2/giphy.gif)
