addSbtPlugin("org.xerial.sbt" % "sbt-pack" % "0.8.0")  // for sbt-0.13.x or higher
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.0")
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "0.8.0")
addSbtPlugin("org.ensime" % "sbt-ensime" % "1.11.3")
